//
//  ScoreManager.swift
//  iOS Quiz
//
//  Created by Ali Almorshed on 16/05/21.
//  Copyright © 2021 Jitender Kumar Yadav. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class ScoreManager {
    public static let shared = ScoreManager()
    var entityData: [ScoreEntity] = []
    var appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
    
 
    func fetchAllData() -> [ScoreEntity] {
            var scoreListEntity: [ScoreEntity] = []
            
            let fetchRequest: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "ScoreEntity")
            let mcqFetchRequest: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "MCQEntity")

        mcqFetchRequest.returnsObjectsAsFaults = false
            do {
                if let results = try self.appDelegate.coreDataStack.managedObjectContext.fetch(fetchRequest) as? [NSManagedObject] {
                    let fetchedData: [ScoreEntity]? = results as? [ScoreEntity]
                    if fetchedData != nil {
                        scoreListEntity = fetchedData!
                        ScoreManager.shared.entityData = scoreListEntity
                        
                    }
                }
            }
            catch {
                fatalError("There was an error fetching the items")
            }
            return scoreListEntity
        }


    
    func saveDataFor(_ scoring: ScoreModel ) {
        if scoring.score != ""{
            var scoreListEntity: [ScoreEntity] = []
            scoreListEntity = ScoreManager.shared.entityData
            
            if scoreListEntity.count == 0{
                scoreListEntity = ScoreManager.shared.fetchAllData()
            }
            let entity: NSEntityDescription? = NSEntityDescription.entity(forEntityName: "ScoreEntity", in: self.appDelegate.coreDataStack.managedObjectContext)
            let subEntity: NSEntityDescription? = NSEntityDescription.entity(forEntityName: "MCQEntity", in: self.appDelegate.coreDataStack.managedObjectContext)

            if entity != nil {
                let newEntity: ScoreEntity = ScoreEntity(entity: entity!, insertInto: self.appDelegate.coreDataStack.managedObjectContext)
                newEntity.score = scoring.score
                newEntity.timeStamp = scoring.timeStamp
                for i in 0...scoring.questions.count-1 {
                    let newSubEntity: MCQEntity = MCQEntity(entity: subEntity!, insertInto: self.appDelegate.coreDataStack.managedObjectContext)
                    newSubEntity.question = scoring.questions[i]
                    newSubEntity.answer = scoring.answers[i]
                    newSubEntity.mark = scoring.individualScore[i]
                    newEntity.addToMcq(newSubEntity)

                }
                 scoreListEntity.append(newEntity)
                self.appDelegate.coreDataStack.saveContext()
            }
            let _ = ScoreManager.shared.fetchAllData()
            }
    }

 
}
