//
//  MCQEntity+CoreDataProperties.swift
//  iOS Quiz
//
//  Created by Ali Almorshed on 16/05/21.
//  Copyright © 2021 Jitender Kumar Yadav. All rights reserved.
//
//

import Foundation
import CoreData


extension MCQEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<MCQEntity> {
        return NSFetchRequest<MCQEntity>(entityName: "MCQEntity")
    }

    @NSManaged public var question: String?
    @NSManaged public var answer: String?
    @NSManaged public var mark: String?
    @NSManaged public var mcq: ScoreEntity?

}

extension MCQEntity : Identifiable {

}
