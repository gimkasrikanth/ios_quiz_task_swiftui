//
//  ScoreEntity+CoreDataProperties.swift
//  iOS Quiz
//
//  Created by Ali Almorshed on 16/05/21.
//  Copyright © 2021 Jitender Kumar Yadav. All rights reserved.
//
//

import Foundation
import CoreData


extension ScoreEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ScoreEntity> {
        return NSFetchRequest<ScoreEntity>(entityName: "ScoreEntity")
    }

    @NSManaged public var score: String?
    @NSManaged public var timeStamp: String?
    @NSManaged public var mcq: NSSet

}

// MARK: Generated accessors for mcq
extension ScoreEntity {

    @objc(addMcqObject:)
    @NSManaged public func addToMcq(_ value: MCQEntity)

    @objc(removeMcqObject:)
    @NSManaged public func removeFromMcq(_ value: MCQEntity)

    @objc(addMcq:)
    @NSManaged public func addToMcq(_ values: NSSet)

    @objc(removeMcq:)
    @NSManaged public func removeFromMcq(_ values: NSSet)

}

extension ScoreEntity : Identifiable {

}
