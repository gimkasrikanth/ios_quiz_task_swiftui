//
//  CustomButton.swift
//  iOS Quiz
//
//  Created by Ali Almorshed on 16/04/21.
//  Copyright © 2021 Jitender Kumar Yadav. All rights reserved.
//

import SwiftUI

struct CustomButton: View {
    @State var action: (()-> Void)?
    @Binding var title: String

    var body: some View {
        Button(action: {
            self.action?()
        }, label: {
            Text(title)
            .foregroundColor(Color.white)
        })
        .frame(width: UIScreen.width*0.45, height: 40)
        .background(Color.black)
        .cornerRadius(5)
        .padding([.bottom], 5)
    }
}


