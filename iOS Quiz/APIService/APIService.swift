//
//  APIService.swift
//  iOS Quiz
//
//  Created by Ali Almorshed on 16/04/21.
//  Copyright © 2021 Jitender Kumar Yadav. All rights reserved.
//

import Foundation


class APIService {
    public static let shared = APIService()
    
    func loadJson(filename fileName: String) -> [QuizModel]? {
        if let url = Bundle.main.url(forResource: fileName, withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let decoder = JSONDecoder()
                let jsonData = try decoder.decode(QuizDataModel.self, from: data)
                return jsonData.quiz
            } catch {
                print("error:\(error)")
            }
        }
        return nil
    }
}
