//
//  QuizViewModel.swift
//  iOS Quiz
//
//  Created by Ali Almorshed on 16/04/21.
//  Copyright © 2021 Jitender Kumar Yadav. All rights reserved.
//

import Foundation
import SwiftUI

class QuizViewModel: NSObject, ObservableObject {
    var questionnaire = [QuizModel]()
    
    var attemptTime = Date()
    
    override init() {
        super.init()
        
        self.questionnaire = APIService.shared.loadJson(filename: "Data") ?? []

    }
    

    func validateMCQs(completionHandler: @escaping (String)-> Void) {
        var marks = 0
        var scoreBoard = ScoreModel()
        
        self.questionnaire.forEach({
            scoreBoard.questions.append($0.question)
            scoreBoard.answers.append($0.selectedAnswer)
            if $0.answer == $0.selectedAnswer{
                marks += 5
                scoreBoard.individualScore.append("5")
            }else{
                marks -= 2
                scoreBoard.individualScore.append("-2")
            }
        })
        scoreBoard.score = "\(marks)"
        let formatter = DateFormatter()
        formatter.timeStyle = .medium
        formatter.dateStyle = .long
        scoreBoard.timeStamp = formatter.string(from: self.attemptTime)
        ScoreManager.shared.saveDataFor(scoreBoard)
        completionHandler("\(marks)")
        
        return

    }

}
