//
//  ResultViewModel.swift
//  iOS Quiz
//
//  Created by Ali Almorshed on 16/04/21.
//  Copyright © 2021 Jitender Kumar Yadav. All rights reserved.
//

import Foundation
import SwiftUI

class ResultViewModel: NSObject, ObservableObject {
    var results = [ScoreEntity]()
        
    override init() {
        super.init()
        
        self.results = ScoreManager.shared.fetchAllData().reversed()
    }
    
}
