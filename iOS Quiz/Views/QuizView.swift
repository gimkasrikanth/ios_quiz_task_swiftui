//
//  QuizView.swift
//  iOS Quiz
//
//  Created by Ali Almorshed on 16/04/21.
//  Copyright © 2021 Jitender Kumar Yadav. All rights reserved.
//

import SwiftUI

struct QuizView: View {
    @Environment(\.safeAreaInsets) private var safeAreaInsets
    @State var quizViewModel = QuizViewModel()
    @State var selectedIndex = 0
    @State var selectedAnswer = ""
    @State var previousTitle = "Previous"
    @State var nextTitle = "Next"
    @State var errorMessage = ""
    @State var marks = ""
    @State private var showingAlert = false

    var body: some View {
        
        ZStack {
            VStack(alignment: .leading) {
                Button(action: {
                    UIApplication.shared.addRootController(view: AnyView(HomeView()))
                }) {
                  HStack {
                    Image(systemName: "chevron.left")
                      .foregroundColor(.blue)
                      .imageScale(.large)
                    Text("Back")
                      .font(.body)
                      .foregroundColor(.blue)
                  }
                }
                .padding(.leading)
                Color.gray.frame(height:CGFloat(1) / UIScreen.main.scale)
                    .padding(.bottom)
                    Text("Q. \(quizViewModel.questionnaire[selectedIndex].question)")
                    Text("MCQ's.")
                    ForEach(quizViewModel.questionnaire[selectedIndex].mCQList, id: \.self) { dt in
                        Button {
                            print(dt)
                            self.errorMessage = ""
                            quizViewModel.questionnaire[selectedIndex].selectedAnswer = dt
                            self.selectedAnswer = dt
                            print(quizViewModel.questionnaire[selectedIndex].selectedAnswer)
                        } label: {
                            HStack{
                                if (quizViewModel.questionnaire[selectedIndex].selectedAnswer == dt) || (self.selectedAnswer == dt){
                                    ZStack{
                                        Circle()
                                            .fill(Color.blue)
                                            .frame(width: 20, height: 20)
                                        Circle()
                                            .fill(Color.white)
                                            .frame(width: 8, height: 8)
                                    }
                                }else{
                                    Circle()
                                        .fill(Color.white)
                                        .frame(width: 20, height: 20)
                                        .overlay(Circle().stroke(Color.gray, lineWidth: 1))
                                }
                                Text(dt)
                                Spacer()
                            }
                        }
                    }
                Text(errorMessage)
                .foregroundColor(Color.red)
                Spacer()
                HStack{
                    if selectedIndex != 0{
                        CustomButton(action: {
                            self.nextTitle = "Next"
                            self.selectedAnswer = ""
                            self.errorMessage = ""
                            if self.selectedIndex > 0{
                                self.selectedIndex -= 1
                            }
                        },title: $previousTitle)
                    }
                    Spacer()
                    CustomButton(action: {
                            self.errorMessage = ""
                            self.selectedAnswer = ""
                        if self.quizViewModel.questionnaire[self.selectedIndex].selectedAnswer != ""{
                            if self.nextTitle == "Submit"{
                                self.quizViewModel.validateMCQs { (marks) in
                                    self.showingAlert = true
                                    self.marks = marks
                                    print(marks)
                                }
                            }else{
                                if self.selectedIndex < (self.quizViewModel.questionnaire.count-1){
                                    self.selectedIndex += 1
                                    if self.selectedIndex == (self.quizViewModel.questionnaire.count-1){
                                        self.nextTitle = "Submit"
                                    }
                                }
                            }
                        }else{
                            self.errorMessage = "Kindly select MCQ"
                        }
                    },title: $nextTitle)
                }
                .padding([.bottom], 20)
            }
            .padding([.leading, .trailing], 5)
            Spacer()

        }
        .alert(isPresented: $showingAlert) {
            Alert(title: Text("Your score is"),
              message: Text(self.marks),
              dismissButton: .default(Text("Ok")){
                    UIApplication.shared.addRootController(view: AnyView(HomeView()))
              })
        }
        .padding(safeAreaInsets)
        .navigationBarHidden(true)
    }
}

struct QuizView_Previews: PreviewProvider {
    static var previews: some View {
        QuizView()
    }
}
