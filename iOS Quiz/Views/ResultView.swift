//
//  ResultView.swift
//  iOS Quiz
//
//  Created by Ali Almorshed on 16/04/21.
//  Copyright © 2021 Jitender Kumar Yadav. All rights reserved.
//

import SwiftUI

struct ResultView: View {
    @State var resultViewModel = ResultViewModel()
    @Environment(\.safeAreaInsets) private var safeAreaInsets

    var body: some View {

            ZStack(alignment: .topLeading) {

                LinearGradient(gradient: Gradient(colors: [Color.white, Color.white]), startPoint: .topLeading, endPoint: .bottomTrailing).edgesIgnoringSafeArea(.all)
                VStack(alignment: .leading){
                    Button(action: {
                        UIApplication.shared.addRootController(view: AnyView(HomeView()))
                    }) {
                      HStack {
                        Image(systemName: "chevron.left")
                          .foregroundColor(.blue)
                          .imageScale(.large)
                        Text("Back")
                          .font(.body)
                          .foregroundColor(.blue)
                      }
                    }
                    .padding(.leading)
                    ZStack{
                        VStack {
                            Color.gray.frame(height:CGFloat(1) / UIScreen.main.scale)
                            HStack {
                                Text("TimeStamp")
                                    .frame(width: UIScreen.width*0.5)
                                Text("Score")
                                .frame(width: UIScreen.width*0.5)

                            }
                            .frame( height: 20)
                            Color.gray.frame(height:CGFloat(1) / UIScreen.main.scale)
                            HStack {
                                ScrollView {
                                    ForEach(resultViewModel.results, id: \.self) { dt in
                                        HStack {
                                                Text(dt.timeStamp ?? "")
                                                    .frame(width: UIScreen.width*0.5)
                                                if (Int(dt.score ?? "") ?? 0) == 25{
                                                    Text(dt.score ?? "")
                                                    .frame(width: UIScreen.width*0.5)
                                                        .foregroundColor(Color.green)
                                                }else if (Int(dt.score ?? "") ?? 0) >= 25/2{
                                                    Text(dt.score ?? "")
                                                    .frame(width: UIScreen.width*0.5)
                                                        .foregroundColor(Color.orange)
                                                }else{
                                                    Text(dt.score ?? "")
                                                    .frame(width: UIScreen.width*0.5)
                                                        .foregroundColor(Color.red)
                                                }
                                        }
                                        .padding([.top,.bottom], 2.5)
                                    }
                                    
                                }.frame(alignment: .leading)
                            }
                        }
                        Color.gray.frame(width:CGFloat(1) / UIScreen.main.scale)
                    }
            }
            }
            
        .navigationBarHidden(true)
        .padding(safeAreaInsets)
            
    }
}

struct ResultView_Previews: PreviewProvider {
    static var previews: some View {
        ResultView()
    }
}
