//
//  HomeView.swift
//  iOS Quiz
//
//  Created by Ali Almorshed on 16/04/21.
//  Copyright © 2021 Jitender Kumar Yadav. All rights reserved.
//

import SwiftUI

struct HomeView: View {
    @State var startTitle = "Start"
    @State var resultTitle = "Result"

    var body: some View {
        NavigationView {

            ZStack{
                LinearGradient(gradient: Gradient(colors: [Color.blue, Color.white]), startPoint: .topLeading, endPoint: .bottomTrailing).edgesIgnoringSafeArea(.all)

                VStack{
                    NavigationLink(destination:  QuizView()) {
                        CustomButton(title: $startTitle)
                    }
                    NavigationLink(destination:  ResultView()) {
                        CustomButton(title: $resultTitle)
                    }
                }
            }
            
            .navigationBarHidden(true)
            
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
