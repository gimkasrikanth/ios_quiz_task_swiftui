//
//  QuizModel.swift
//  iOS Quiz
//
//  Created by Ali Almorshed on 16/04/21.
//  Copyright © 2021 Jitender Kumar Yadav. All rights reserved.
//

import Foundation
import SwiftUI

class QuizDataModel : Codable {
    var quiz : [QuizModel] = []

    enum CodingKeys: String, CodingKey {

        case quiz = "Quiz"
    }

    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        quiz = try values.decodeIfPresent([QuizModel].self, forKey: .quiz) ?? []
    }

}

class QuizModel: Identifiable, Codable {
    var id = UUID()
    var question : String = ""
    var answer : String = ""
    var selectedAnswer : String = ""
    var mCQList : [String] = []

    enum CodingKeys: String, CodingKey {
        case question = "Question"
        case mCQList = "MCQ Answer"
        case answer = "Answer"
    }
    
    required init(from decoder: Decoder) throws {
    let values = try decoder.container(keyedBy: CodingKeys.self)

        question = try (values.decodeIfPresent(String.self, forKey: .question) ?? "")
        mCQList = try (values.decodeIfPresent([String].self, forKey: .mCQList) ?? [])
        answer = try (values.decodeIfPresent(String.self, forKey: .answer) ?? "")

    }

    init() {
        
    }


}
