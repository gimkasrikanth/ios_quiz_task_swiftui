//
//  ScoreModel.swift
//  iOS Quiz
//
//  Created by Ali Almorshed on 16/04/21.
//  Copyright © 2021 Jitender Kumar Yadav. All rights reserved.
//

import Foundation


struct ScoreModel {
    var score = ""
    var timeStamp = ""
    var questions = [String]()
    var answers = [String]()
    var individualScore = [String]()
}
